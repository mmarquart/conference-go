from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests

"""
TODO:
Need to write two functions, one that makes a request to use the Pexels API, and one to make a request to the Open Weather API. Then, use those functions in your views.

"""


def get_image_from_pexels(query):
    api_key = PEXELS_API_KEY
    url = "https://api.pexels.com/v1/search"

    headers = {"Authorization": api_key}

    params = {
        "query": query,
        "per_page": 1,
        "page": 1,
    }

    response = requests.get(url, headers=headers, params=params)

    if response.status_code == 200:
        data = response.json()
        if data["photos"]:
            image_url = data["photos"][0]["src"]["original"]
            return image_url
    else:
        print(f"Error: {response.status_code}")
        return None



